library(tidyverse)
library(readxl)
library(tidytext)
library(ggraph)
library(stopwords)

source("functions.R")

# Reading data
data <- 
  readxl::read_xlsx("data/learning_text_data.xlsx") %>% 
  dplyr::mutate(learning = as.factor(learning)) %>% 
  dplyr::mutate(descricao = prep_fun(descricao))


####
# BIGRAM
###

# Remove stopwords before bigram
stopwords_regex <- paste0('\\b', paste(stopwords('en'), collapse = '\\b|\\b'), '\\b')

# Cleaning data
data_clean <-
  data %>%
  # Retirando stop words
  dplyr::mutate(descricao = stringr::str_replace_all(descricao, stopwords_regex, " ")) %>% 
  dplyr::mutate(descricao = stringr::str_replace_all(descricao, "[0-9]+", " "))  %>% 
  dplyr::mutate(descricao = stringr::str_replace_all(descricao, " *\\b[[:alpha:]]{1,2}\\b *", " "))
  
# Create and export bi-gram charts
bigram <-
lapply(as.character(unique(data$learning)), 
       function(x){
         # Obter o sentimento de cada par de palavra
         bigram_data <- data_clean %>% 
           dplyr::filter(learning == paste0(x)) %>% 
           # criar um token para cada palavra escrita em cada tipo
           tidytext::unnest_tokens(bigram, descricao, token = "ngrams", n = 2)
         
         # Par de palavras mais frequentes (observe a frequência com que stop words aparecem)
         bigram_counts <- bigram_data %>%
           dplyr::count(bigram, sort = TRUE)
         
         # Bigram sem stop words
         bigram_clean_data <- bigram_data %>%
           # separar o par de palavras em duas colunas
           tidyr::separate(bigram, c("word1", "word2"), sep = " ")
         
         # Par de palavras mais frequentes (clean)
         bigram_counts_clean <- bigram_clean_data %>%
           dplyr::count(word1, word2, sort = TRUE)
         
         # Visualizar relação entre as palavras
         bigram_graph <- bigram_counts_clean %>%
           dplyr::filter(n >= 2) %>%
           igraph::graph_from_data_frame()
         
         graph <- 
         ggraph::ggraph(bigram_graph, layout = "fr") +
           ggraph::geom_edge_link(color = "blue") +
           ggraph::geom_node_point(color = "blue") +
           ggraph::geom_node_text(aes(label = name), vjust = .5, hjust = .5, check_overlap = TRUE, repel = TRUE) +
           ggtitle(paste("Words co-occurences:", x, "learning")) +
           theme_void() +
           ggraph::theme_graph(
             base_family = "Times New Roman",
             background = "white",
             foreground = NULL,
             border = TRUE,
             title_size = 12,
             title_face = "bold",
             subtitle_face = "plain",
             plot_margin = margin(15, 15, 15, 15)
           ) +
           set_graph_style(text_size = 11) 
         
         png(filename=paste0("images/ngram_", x ,".png"), width = 240, height = 280, )
         plot(graph)
         dev.off()
         
         graph
       }
)

# Rename objects in list
names(bigram) <- as.character(unique(data$learning))


